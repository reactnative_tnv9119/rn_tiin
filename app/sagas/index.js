/**
 *  Redux saga class init
 */
import { takeEvery, all, put } from 'redux-saga/effects';
import * as types from '../actions/types';
import loginSaga from './loginSaga';
import * as loginActions from '../actions/loginActions';

export default function* watch() {
  try {
    yield all([takeEvery(types.LOGIN_REQUEST, loginSaga)]);
  } catch (error) {
    // dispatch a failure action to the store with the error
    yield put(loginActions.loginFailed());
  }
}
